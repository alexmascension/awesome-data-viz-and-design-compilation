# Awesome data viz and design compilation 📊
 
In this project I will upload any relevant content that is useful for data design and visualization. Many times I get stuck choosing color palettes, configuring charts, or looking for inspiration in how to make my data look better. Many times I find interesting websites about color theory, color maps, recommendations on data visualization, concepts of design, etc., and many times I forget where I took this information from. In order to make that process easier, I will include webs, resources, and even my contributions to this repo, and make good data visualization affordable and not a nightmare. So resources I haven't seen them in depth, but I have added them becuase I found them satisfying.


## LaTeX resources 📄
- A (finally) beautiful LaTeX beamer theme: [**mtheme**](https://github.com/matze/mtheme). The theme is simple, with nice color selection, dark / light color schemes, interesting features (like redesigning the `alert` and `example` blocks).

## Color palettes 🎨
- A good palette corrector: [**Chroma.js Color Palette Helper**](https://gka.github.io/palettes). This tool lets you add a set of colors, and generates a new luminance- and saturation-corrected palette.
- A comprenhensive set of palettes: [**Color Brewer 2**](http://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3). The common set of palettes, with nice interactive visualizations. Offers palettes for different grades of daltonism, and printer-friendly palettes.
- A more modern set of palettes: [**Carto colors**](https://carto.com/carto-colors/). It includes sequential, diverging and qualitative palettes, with around 10 colors each. **Really nice choices of palletes**.
- A tool to create gradients: [**HCL picker**](http://tristen.ca/hcl-picker). This tool generates a colormap (you can choose between Hue-Lightness, Chroma-Lightness and Hue-Chroma to show the maps); you choose 2 points, and it generates a scale of discrete colors.
- Simple gradient generator: [**Learn UI Data Color Picker**](https://learnui.design/tools/data-color-picker.html#palette). This tool generates different color palettes. Although it is much simpler than others, palettes are nice.
- Clustering of colors: [**I want hue**](https://medialab.github.io/iwanthue/). In this tool you choose a range of colors based on lightness, hue and saturation, and it returns a set of colors, which are clusteres using k-means algorithm. The final colors are a combination of the colors from each cluster. The tool is quite complex, and the colors generated can be of more reduced quality compared to profesionally-designed palettes, but is a good tool to play with.

## Evaluate your colors ✅
- [**Viz Palette**](https://projects.susielu.com/viz-palette) Do you want to see how your palette works on data, but don't want to get hands on writing code to do so? Viz Palette borns out of this frustration and shows a toolkit to evaluate palettes: different graphs (batplots, lineplots, violins, scatters) with and without strokes, a report of colors which shows conflicts between colors. It also allows you to change the color of fonts and backgrounds, to see contrast in the figures.
- [**Is your contrast good?**](https://color.review/). This web lets you add two colors and evaluate their contrast. There are 3 contrast thresholds based on a equation from the [Web Content Accessibility Guidelines](https://www.w3.org/WAI/standards-guidelines/wcag/). Depending on the importance of the text (header or main text, size, etc.) certain thresholds are favoured, even more for people with visual impairments.

## Easy web reads (< 5 min) 📗
- [**Color in UI**](https://learnui.design/blog/color-in-ui-design-a-practical-framework.html). This post shows simple details on how to choose similar colors do design UI features. It is also interesting for any other aplication, because the concepts are easy to grasp, and really useful!
- [**A primer on HSB**](https://learnui.design/blog/the-hsb-color-system-practicioners-primer.html). This post shows basics on color theory, but shows really interesting remarks, like **how to improve dark color gradients by not using black**.

## Medium web reads (5 - 20 min) 📙
- [**Movies in color**](https://moviesincolor.com/). A good recopilation of color analysis of films.
- [**Visualising data: make grey your best friend**](https://www.visualisingdata.com/2015/01/make-grey-best-friend/). A good analysis on why gray is important as a resource on plot visualization.

## Longer web reads (> 20 min) 📕
- [**Programming Design Systems**](https://programmingdesignsystems.com/). This web is a online book (still being written) that shows topics on color theory, color schemes, and logic of shapes and sizes.

## Books 📚
This resources are books (usually in pdf) that I found interesting, or that I have yet to read (but that doesn't stop you to do so!).
- [**ITTEN - The elements of color**](https://monoskop.org/images/4/46/Itten_Johannes_The_Elements_of_Color.pdf) 

## Inspiration 💡
Sometimes the best way to start designing is looking at existing designs! Here there are webs and resources for inspiration on design.
- [**Designpiration**](https://www.designspiration.com/)

## Colors from Films 🎞
- [**TCOM - The Colors of Motion**](https://thecolorsofmotion.com/) This web shows examples of colors of films by frame. Useful to find trends and color palettes from groups of authors!

## Branched repos / web resources 🌱
### Color theory and design 🌈
Webs and resources that have many sections to be explored.
- [**Google Arts and Culture**](https://artsandculture.google.com/). This web includes many many sections about art schools and design rules. There is a great influence on the Bauhaus school of design, which I found really inspiring.
- [**Material Design**](https://material.io/design/). This web is a comprenhensive review on color theory for UI, layout, typography, iconography. Amazing!
### Data analysis 📉
- [**Visualising data**](https://www.visualisingdata.com/). Good web to see examples of visualizations done right.
- [**Information is Beautiful Awards**](https://www.informationisbeautifulawards.com/showcase?award=2019&type=awards). Interesting resource to view good charts.
- [**FlowingData**](https://flowingdata.com/). This is a blog by Nathan Yau. It includes many examples of visualizations from data analysis. 
### Others 🧲
- [**AutoDraw**](https://www.autodraw.com/) This is a Google AI that guesses what you are drawing and lets you select from a set of svg files of the best guesses!

## Sources 🔗
https://blog.datawrapper.de/colorguide/. Many, almost all resources are taken from here. Shoutout to Lisa Charlotte Rost on the work! 
